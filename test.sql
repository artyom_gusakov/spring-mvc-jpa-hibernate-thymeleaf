﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.78.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 28.11.2017 14:50:17
-- Версия сервера: 5.7.18-log
-- Версия клиента: 4.1
--


--
-- Описание для базы данных test
--
DROP DATABASE IF EXISTS test;
CREATE DATABASE test
	CHARACTER SET utf8
	COLLATE utf8_general_ci;

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

-- 
-- Установка базы данных по умолчанию
--
USE test;

--
-- Описание для таблицы traffic_direction
--
CREATE TABLE traffic_direction (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы traffic_stat
--
CREATE TABLE traffic_stat (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  traffic_date TIMESTAMP NULL DEFAULT NULL,
  traffic_direction INT(11) DEFAULT NULL,
  user_id BIGINT(20) DEFAULT NULL,
  traffic_amount DOUBLE DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_traffic_stat_traffic_direction_id FOREIGN KEY (traffic_direction)
    REFERENCES traffic_direction(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FK_traffic_stat_users_id FOREIGN KEY (user_id)
    REFERENCES users(id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 21
AVG_ROW_LENGTH = 862
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы users
--
CREATE TABLE users (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  surname VARCHAR(255) NOT NULL,
  patronymic VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

-- 
-- Вывод данных для таблицы traffic_direction
--
INSERT INTO traffic_direction VALUES
(1, 'uplink'),
(2, 'downlink');

-- 
-- Вывод данных для таблицы traffic_stat
--
INSERT INTO traffic_stat VALUES
(1, '2017-11-27 14:20:56', 1, 1, 65654.15),
(2, '2017-11-27 14:21:18', 2, 1, 64653),
(3, '2017-11-28 08:34:19', 1, 1, 15),
(4, '2017-11-28 08:34:30', 1, 1, 30),
(5, '2017-11-28 08:34:43', 2, 1, 30),
(6, '2017-11-28 08:34:53', 2, 1, 50),
(7, '2017-11-28 08:35:03', 1, 1, 20),
(9, '2017-11-29 08:34:19', 1, 2, 15),
(10, '2017-11-29 08:34:44', 1, 2, 30),
(11, '2017-11-28 14:47:44', 2, 3, 45),
(12, '2017-11-29 14:47:58', 2, 3, 4554),
(13, '2017-11-29 14:48:01', 1, 3, 45453453),
(14, '2017-11-30 14:48:16', 1, 3, 68),
(15, '2017-11-29 14:48:38', 1, 3, 66),
(16, '2017-11-30 14:48:51', 1, 3, 56),
(17, '2017-11-29 14:49:02', 2, 3, 33),
(18, '2017-11-29 14:49:12', 2, 3, 666),
(19, '2017-11-29 14:49:26', 1, 2, 36),
(20, '2017-11-30 14:49:36', 2, 2, 888);

-- 
-- Вывод данных для таблицы users
--
INSERT INTO users VALUES
(1, 'Иванов', 'Иван', 'Иванович'),
(2, 'Петров', 'Петр', 'Петрович'),
(3, 'Борисов', 'Борис', 'Борисович');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/**
 * Created by sokol on 25.01.2017.
 * Прелоадер:
 *      falconga_preloader.js
 *      falconga_preloader.css
 *      preloader.svg
 * Функционал:
 *      Выводится прелоадер. Затемняется весь экран, в центре выводится svg-изображение
 *      При выводе нескольких прелоадеров они не будут влиять друг на друга.
 * Зависимости:
 *      jQuery jQuery v1.11.1
 *      Bootstrap v3.3.6
 * Что нужно сделать, чтобы всё заработало:
 *      1. Уставновить все зависимости
 *      2. Для вывода прелоадера вызывать функцию showPreloader([<HTMLContainer>]):
 *          HTMLContainer - необязательный параметр, определяет HTML-контейнер, где находится прелоадер
 *      3. Для сокрытия прелоадера вызвать функцию hidePreloader([<HTMLContainer>])
 * Переменные:
 *      1. FGA__PRELOADER_WRAPPER_ID - id HTML-контейнера, где находится прелоадер
 *      2. FGA__PRELOADER_WRAPPER_CLASS - CSS-класс HTML-контейнера, где находится прелоадер
 *      3. FGA__PRELOADER_IMAGE_CLASS - CSS-класс HTML-контейнера, где находится изображение прелоадера
 *
 * Задачи
 *      1. Добавить возможноть выбора нескольких прелоадеров (столбчатый, линейный, круговой...)
 *      2. Возможность запуска прелоадер в любом HTML-элементе
 */

function Fga__PoolOfPreloaders() {
    /*
     * Пул прелоадеров
     * */
    this.preloaders = {};
    this.listOfUIDs = [];
    this.pushPreloader = function(preloader) {
        if (this.preloaders[preloader] == undefined) {
            this.preloaders[preloader] = 1;
        } else {
            this.preloaders[preloader] += 1;
        }
    };
    this.popPreloader = function(preloader) {
        if (this.preloaders[preloader]) {
            this.preloaders[preloader] -= 1;
            if (!this.preloaders[preloader]) {
                this.preloaders[preloader] = undefined;
            }
        }
    };
    this.hasPreloader = function(preloader) {
        if (this.preloaders[preloader] == 0) return false;
        return true;
    };
    this.getUID = function(string) {
        var position = this.listOfUIDs.indexOf(string);
        if (!~position) {
            this.listOfUIDs.push(string);
            return 'fga-preloader__insider-' + (this.listOfUIDs.length - 1).toString();
        } else {
            return 'fga-preloader__insider-' + position.toString();
        }
    }
}

var fga__preloaders = 0;
var FGA__PRELOADER_WRAPPER_ID = 'fga__preloader';
var FGA__PRELOADER_WRAPPER_CLASS = 'fga-preloader-wrapper fga-preloader';
var FGA__PRELOADER_IMAGE_CLASS = 'fga-preloader__image';
var FGA__PRELOADER_BACKGROUND_COLOR = 'rgba(0, 0, 0, 0.6)';
var FGA__PRELOADER_TYPE = 'default';
var FGA_PRELOADER_TYPES = {
    'default': 'preloader__default',
    'audio': 'preloader__audio',
    'ball-triangle': 'preloader__ball-triangle',
    'bars': 'preloader__bars',
    'circles': 'preloader__circles',
    'grid': 'preloader__grid',
    'hearts': 'preloader__hearts',
    'oval': 'preloader__oval',
    'puff': 'preloader__puff',
    'spinning-circles': 'preloader__spinning-circles',
    'tail-spin': 'preloader__tail-spin',
    'three-dots': 'preloader__three-dots'
};

//Тестовая переменная - пул прелоадеров
var fga__poolOfPreloaders = new Fga__PoolOfPreloaders();

(function init() {
    $('body').append('<div id="' + FGA__PRELOADER_WRAPPER_ID + '" class="' + FGA__PRELOADER_WRAPPER_CLASS + '"><div class="' + FGA__PRELOADER_IMAGE_CLASS + '"></div></div>');
})();

function showPreloader(options) {
    var backgroundColor = (options && options.backgroundColor) ? options.backgroundColor : FGA__PRELOADER_BACKGROUND_COLOR;
    var type = (options && options.type) ? options.type : FGA__PRELOADER_TYPE;
    var parentContainer = (options && options.HTMLContainer) ? options.HTMLContainer : undefined;
    var dark = (options && options.dark) ? '_dark' : '';
    var block_id = FGA__PRELOADER_WRAPPER_ID;

    if (parentContainer != undefined) {
        block_id = fga__poolOfPreloaders.getUID(btoa(parentContainer.attr('class') + ':' + parentContainer.attr('block')));
        parentContainer.append('' +
            '<div id="' + block_id + '" class="fga-preloader__insider" style="background-color: ' + backgroundColor + '">' +
                '<div class="' + FGA__PRELOADER_IMAGE_CLASS + '"></div>' +
            '</div>'
        );
    }

    fga__poolOfPreloaders.pushPreloader(block_id);

    HTMLContainer = $('#' + block_id);
    var height = getHeightByParent(HTMLContainer.height());
    HTMLContainer.css('backgroundColor', backgroundColor);
    $('.' + FGA__PRELOADER_IMAGE_CLASS, HTMLContainer).addClass(FGA_PRELOADER_TYPES[type] + dark);
    $('.' + FGA__PRELOADER_IMAGE_CLASS, HTMLContainer).css('backgroundSize', 'auto ' + height);
    HTMLContainer.show();
}
function hidePreloader(options) {
    var parentContainer = (options && options.HTMLContainer) ? options.HTMLContainer : undefined;
    var block_id = FGA__PRELOADER_WRAPPER_ID;

    if (parentContainer != undefined) {
        block_id = fga__poolOfPreloaders.getUID(btoa(parentContainer.attr('class') + ':' + parentContainer.attr('block')));
    }

    //Вычитаем один из стандартных
    HTMLContainer = $('#' + block_id);
    fga__poolOfPreloaders.popPreloader(block_id);
    if (fga__poolOfPreloaders.hasPreloader(block_id)) {
        HTMLContainer.hide();
        //Если это не основной контейнер - удалим его
        if (parentContainer) HTMLContainer.remove();
    }
}

function getHeightByParent(height) {
    "use strict";
    if (height > 0 && height < 500) return '50px';
    if (height > 500 && height < 1000) return '100px';
    if (height > 1000 && height < 1500) return '200px';
    if (height > 1500) return '250px';
}
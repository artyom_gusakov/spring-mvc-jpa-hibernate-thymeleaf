var TRAFFIC_STATISTIC = {};
$(document).on('click', '.submit-button', function (e) {
    var request = {};
    var userId = $(".user-list").val();
    var trafficDirectId = $(".trafficDir-list").val();
    var periodStart = $(".period-start").val();
    var periodEnd = $(".period-end").val();

    if ((periodStart == "" || !periodStart) || (periodEnd == "" || !periodEnd)) {
        $('.error-text').html('Проверьте корректность заполнения периодов для отчета');
        $(".error-message").show(300);
        return;
    }

    var start = moment(periodStart, "DD.MM.YYYY HH:mm:ss");
    var end = moment(periodEnd, "DD.MM.YYYY HH:mm:ss");
    var isAfter = start > end;

    if (isAfter) {
        $('.error-text').html('Значение поля "Окончание отчетного периода" должно быть больше чем "Начало отчетного периода"');
        $(".error-message").show(300);
        return;
    }

    request = {
        userId: userId,
        trafficDirectId: trafficDirectId,
        periodStartStr: periodStart,
        periodEndStr: periodEnd
    };

    $('.user-traffic-info-card__detail-btn-hide').click();
    $('.user-traffic-info-card').hide(300);
    $.ajax({
        method: "POST",
        url: "/springsite/api/getStatistic",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(request),
        dataTypes: "json",
        beforeSend: function() {
            showPreloader();
        },
        success: function (data) {
            setTimeout(function() {hidePreloader();}, 500);

            //если нет статистики
            if(!data.userFio){
                $('.info-message-text').html('По заданным критериям нет статистики');
                $('.info-message').show(300);
                return;
            }

            TRAFFIC_STATISTIC = data;
            $('.user-traffic-info-card__header').html('<h3>' + data.userFio + '</h3>');

            var html = '<tr><td>Отчетный период:</td><td>' + data.periodStartStr + ' - ' + data.periodEndStr + '</td></tr>';
            html += '<tr><td>Направление трафика:</td><td>' + data.trafficDirectName + '</td></tr>';
            html += '<tr><td>Пропускная способность:</td><td><strong>' + data.speed.toFixed(2) + ' бит/с</strong></td></tr>';
            $('.user-traffic-info-card__table').html(html);

            $('.user-traffic-info-card').show(300);
        },
        error: function (e) {
            console.log(e);
            setTimeout(function() {hidePreloader();}, 500);
        }
    })
});

$(document).on('click', '.user-traffic-info-card__detail-btn-open', function (e) {
    var detail = TRAFFIC_STATISTIC.detail;
    var html = '';
    for (var i = 0; i < detail.length; i++) {
        var detailItem = detail[i];
        html += '<tr><th scope="row">' + (i + 1) + '</th>';
        html += '<td>' + detailItem.dateStr + '</td>';
        html += '<td>' + detailItem.trafficAmount + '</td></tr>';


    }
    $('.traffic-detail__table').html(html);
    $('.user-traffic-info-card__detail-btn-open').hide();
    $('.user-traffic-info-card__detail-btn-hide').show();
    $('.user-traffic-info-card__detail').show(300);

});
$(document).on('click', '.user-traffic-info-card__detail-btn-hide', function (e) {
    $('.user-traffic-info-card__detail-btn-open').show();
    $('.user-traffic-info-card__detail-btn-hide').hide();
    $('.user-traffic-info-card__detail').hide(300);
});
$(document).on('click', '.close-error-btn', function (e) {
    $(".error-message").hide(300);
});

$(document).on('click', '.close-info-message-btn', function (e) {
    $(".info-message").hide(300);
});
$(document).on('click', '.user-traffic-info-card__close-card', function (e) {
    $(".user-traffic-info-card").hide(300);
});
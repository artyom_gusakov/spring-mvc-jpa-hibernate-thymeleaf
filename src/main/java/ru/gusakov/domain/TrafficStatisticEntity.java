package ru.gusakov.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "traffic_stat")
public class TrafficStatisticEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "traffic_date")
    private Timestamp dateTime;

    @Column(name = "traffic_amount")
    private Double trafficAmount;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "traffic_direction", referencedColumnName = "id")
    private TrafficDirectionEntity trafficDirection;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public Double getTrafficAmount() {
        return trafficAmount;
    }

    public void setTrafficAmount(Double trafficAmount) {
        this.trafficAmount = trafficAmount;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public TrafficDirectionEntity getTrafficDirection() {
        return trafficDirection;
    }

    public void setTrafficDirection(TrafficDirectionEntity trafficDirection) {
        this.trafficDirection = trafficDirection;
    }

    @Override
    public String toString() {
        return "TrafficStatisticEntity{" +
                "id=" + id +
                ", dateTime=" + dateTime +
                ", trafficAmount=" + trafficAmount +
                ", user=" + user +
                ", trafficDirection=" + trafficDirection +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrafficStatisticEntity)) return false;

        TrafficStatisticEntity that = (TrafficStatisticEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (dateTime != null ? !dateTime.equals(that.dateTime) : that.dateTime != null) return false;
        if (trafficAmount != null ? !trafficAmount.equals(that.trafficAmount) : that.trafficAmount != null)
            return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return trafficDirection != null ? trafficDirection.equals(that.trafficDirection) : that.trafficDirection == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (trafficAmount != null ? trafficAmount.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (trafficDirection != null ? trafficDirection.hashCode() : 0);
        return result;
    }

}

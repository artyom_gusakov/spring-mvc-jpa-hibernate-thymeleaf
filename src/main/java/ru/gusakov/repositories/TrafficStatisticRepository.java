package ru.gusakov.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.gusakov.domain.TrafficStatisticEntity;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface TrafficStatisticRepository extends CrudRepository<TrafficStatisticEntity, Long> {
    @Query("select s from TrafficStatisticEntity s " +
            "where s.user.id = :uid and s.trafficDirection.id = :tdid and s.dateTime between :start and :finish order by s.dateTime asc")
    List<TrafficStatisticEntity> findStatisticByUserAndDirectionForPeriod(@Param("uid") Long userId,
                                                                                 @Param("tdid") Integer trafficDirectionId,
                                                                                 @Param("start") Timestamp periodStart,
                                                                                 @Param("finish") Timestamp periodEnd);
}

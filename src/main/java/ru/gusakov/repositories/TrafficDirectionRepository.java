package ru.gusakov.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gusakov.domain.TrafficDirectionEntity;

@Repository
public interface TrafficDirectionRepository extends CrudRepository<TrafficDirectionEntity, Integer> {
    TrafficDirectionEntity findByName(String name);
}

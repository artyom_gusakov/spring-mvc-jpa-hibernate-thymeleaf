package ru.gusakov.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gusakov.domain.UserEntity;

@Repository
public interface UserRepository  extends CrudRepository<UserEntity, Long>{
}

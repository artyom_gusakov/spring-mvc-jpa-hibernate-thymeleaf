package ru.gusakov.dto;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class StatisticInfoCardView {
    private String userFio;
    private String periodStartStr;
    private String periodEndStr;
    private String trafficDirectName;
    private Double speed;
    private List<StatisticDetail> detail = new ArrayList<>();


    public String getUserFio() {
        return userFio;
    }

    public void setUserFio(String userFio) {
        this.userFio = userFio;
    }

    public String getPeriodStartStr() {
        return periodStartStr;
    }

    public void setPeriodStartStr(String periodStartStr) {
        this.periodStartStr = periodStartStr;
    }

    public String getPeriodEndStr() {
        return periodEndStr;
    }

    public void setPeriodEndStr(String periodEndStr) {
        this.periodEndStr = periodEndStr;
    }

    public String getTrafficDirectName() {
        return trafficDirectName;
    }

    public void setTrafficDirectName(String trafficDirectName) {
        this.trafficDirectName = trafficDirectName;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public List<StatisticDetail> getDetail() {
        return detail;
    }

    public void setDetail(List<StatisticDetail> detail) {
        this.detail = detail;
    }

    public static class StatisticDetail {
        private String dateStr;
        private Double trafficAmount;

        public String getDateStr() {
            return dateStr;
        }

        public void setDateStr(Timestamp date) {
            this.dateStr = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(date);
        }

        public Double getTrafficAmount() {
            return trafficAmount;
        }

        public void setTrafficAmount(Double trafficAmount) {
            this.trafficAmount = trafficAmount;
        }
    }
}

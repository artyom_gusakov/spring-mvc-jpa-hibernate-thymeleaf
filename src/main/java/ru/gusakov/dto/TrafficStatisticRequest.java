package ru.gusakov.dto;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class TrafficStatisticRequest {

    private Long userId;
    private Integer trafficDirectId;
    private String periodStartStr;
    private String periodEndStr;
    private Timestamp periodStart;
    private Timestamp periodEnd;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getTrafficDirectId() {
        return trafficDirectId;
    }

    public void setTrafficDirectId(Integer trafficDirectId) {
        this.trafficDirectId = trafficDirectId;
    }

    public String getPeriodStartStr() {
        return periodStartStr;
    }

    public void setPeriodStartStr(String periodStartStr) {
        this.periodStartStr = periodStartStr;
    }

    public String getPeriodEndStr() {
        return periodEndStr;
    }

    public void setPeriodEndStr(String periodEndStr) {
        this.periodEndStr = periodEndStr;
    }

    public Timestamp getPeriodStart() throws ParseException{
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date date = formatter.parse(this.getPeriodStartStr());
        Timestamp timeStampDate = new Timestamp(date.getTime());
        return timeStampDate;
    }

    public Timestamp getPeriodEnd() throws ParseException{
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date date = formatter.parse(this.getPeriodEndStr());
        Timestamp timeStampDate = new Timestamp(date.getTime());
        return timeStampDate;
    }



    @Override
    public String toString() {
        return "TrafficStatisticRequest{" +
                "userId=" + userId +
                ", trafficDirectId=" + trafficDirectId +
                ", periodStartStr='" + periodStartStr + '\'' +
                ", periodEndStr='" + periodEndStr + '\'' +
                ", periodStart=" + periodStart +
                ", periodEnd=" + periodEnd +
                '}';
    }
}

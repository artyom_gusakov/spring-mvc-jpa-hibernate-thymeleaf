package ru.gusakov.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.gusakov.services.TrafficDirectionService;
import ru.gusakov.services.UserService;

@Controller
public class MainController {

    private UserService userService;

    private TrafficDirectionService trafficDirectionService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTrafficDirectionService(TrafficDirectionService trafficDirectionService) {
        this.trafficDirectionService = trafficDirectionService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap map) {
        map.put("trafficDirections", trafficDirectionService.findAll());
        map.put("users", userService.findAll());
        return "index";
    }


}

package ru.gusakov.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gusakov.domain.TrafficStatisticEntity;
import ru.gusakov.dto.StatisticInfoCardView;
import ru.gusakov.dto.TrafficStatisticRequest;
import ru.gusakov.exceptions.BadRequestException;
import ru.gusakov.services.TrafficStatisticService;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MainRestController {

    private TrafficStatisticService trafficStatisticService;

    @Autowired
    public void setTrafficStatisticService(TrafficStatisticService trafficStatisticService) {
        this.trafficStatisticService = trafficStatisticService;
    }

    @PostMapping(value = "/getStatistic", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public StatisticInfoCardView getStatistic(@RequestBody TrafficStatisticRequest request) {
        List<TrafficStatisticEntity> statistic;
        try {
            statistic = trafficStatisticService.getTrafficStatistic(request);
        } catch (ParseException e) {
            throw new BadRequestException(e.getMessage());
        }
        StatisticInfoCardView view = new StatisticInfoCardView();
        if (statistic.size() > 0) {
            String fio = statistic.get(0).getUser().getSurname() + " " +
                    statistic.get(0).getUser().getName() + " " +
                    statistic.get(0).getUser().getPatronymic();

            String trafficDirectName = statistic.get(0).getTrafficDirection().getName();
            view.setUserFio(fio);
            view.setTrafficDirectName(trafficDirectName);
            view.setPeriodStartStr(request.getPeriodStartStr());
            view.setPeriodEndStr(request.getPeriodEndStr());

            //Получаем скорость соединения если в этот период было более одного соединения
            if (statistic.size() > 1) {
                //количество времени в секундах
                Long seconds = (statistic.get(statistic.size() - 1).getDateTime().getTime() - statistic.get(0).getDateTime().getTime()) / 1000;
                //количество трафика за это время
                Double totalTrafficAmount = statistic.stream().mapToDouble(value -> value.getTrafficAmount()).sum();
                //переводим в бит/сек
                if (seconds > 0)
                    view.setSpeed((totalTrafficAmount / seconds) * 8);
                else view.setSpeed(totalTrafficAmount * 8);
            }
            //Если соединение было одно получаем мгновенную скорость
            else {
                Double totalTrafficAmount = statistic.stream().mapToDouble(value -> value.getTrafficAmount()).sum();
                view.setSpeed(totalTrafficAmount * 8);
            }

            //Формируем детальную информациию по статистике
            statistic.stream().forEach(item -> {
                StatisticInfoCardView.StatisticDetail detail = new StatisticInfoCardView.StatisticDetail();
                detail.setDateStr(item.getDateTime());
                detail.setTrafficAmount(item.getTrafficAmount());
                view.getDetail().add(detail);
            });

        }
        return view;
    }
}
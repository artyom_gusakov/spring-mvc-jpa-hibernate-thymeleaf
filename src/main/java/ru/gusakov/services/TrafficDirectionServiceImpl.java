package ru.gusakov.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.gusakov.domain.TrafficDirectionEntity;
import ru.gusakov.repositories.TrafficDirectionRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrafficDirectionServiceImpl implements TrafficDirectionService {

    private TrafficDirectionRepository repository;

    @Autowired
    public void setRepository(TrafficDirectionRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<TrafficDirectionEntity> findAll() {
        return (ArrayList<TrafficDirectionEntity>) repository.findAll();
    }

    @Override
    public TrafficDirectionEntity findById(int id) {
        return repository.findOne(id);
    }

    @Override
    public TrafficDirectionEntity findByName(String name) {
        return repository.findByName(name);
    }
}

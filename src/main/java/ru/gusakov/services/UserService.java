package ru.gusakov.services;

import ru.gusakov.domain.UserEntity;

import java.util.List;

public interface UserService {
    List<UserEntity> findAll();
}

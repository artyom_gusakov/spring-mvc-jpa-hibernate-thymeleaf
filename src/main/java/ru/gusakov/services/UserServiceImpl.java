package ru.gusakov.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gusakov.domain.UserEntity;
import ru.gusakov.repositories.UserRepository;

import java.util.List;

@Service
@Repository
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserEntity> findAll() {
        return (List<UserEntity>) userRepository.findAll();
    }
}

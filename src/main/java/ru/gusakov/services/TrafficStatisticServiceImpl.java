package ru.gusakov.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.gusakov.domain.TrafficDirectionEntity;
import ru.gusakov.domain.TrafficStatisticEntity;
import ru.gusakov.domain.UserEntity;
import ru.gusakov.dto.TrafficStatisticRequest;
import ru.gusakov.repositories.TrafficDirectionRepository;
import ru.gusakov.repositories.TrafficStatisticRepository;
import ru.gusakov.repositories.UserRepository;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

@Service
public class TrafficStatisticServiceImpl implements TrafficStatisticService {

    private TrafficDirectionRepository trafficDirectionRepository;
    private UserRepository userRepository;
    private TrafficStatisticRepository trafficStatisticRepository;

    @Autowired
    public void setTrafficDirectionRepository(TrafficDirectionRepository trafficDirectionRepository) {
        this.trafficDirectionRepository = trafficDirectionRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setTrafficStatisticRepository(TrafficStatisticRepository trafficStatisticRepository) {
        this.trafficStatisticRepository = trafficStatisticRepository;
    }

    @Override
    public List<TrafficStatisticEntity> getTrafficStatistic(TrafficStatisticRequest request) throws ParseException {
        Timestamp start = request.getPeriodStart();
        Timestamp end = request.getPeriodEnd();

        List<TrafficStatisticEntity> statistics =
                trafficStatisticRepository.findStatisticByUserAndDirectionForPeriod(
                        request.getUserId(), request.getTrafficDirectId(), start, end);
        return statistics;
    }
}

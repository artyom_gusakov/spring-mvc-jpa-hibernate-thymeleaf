package ru.gusakov.services;


import ru.gusakov.domain.TrafficDirectionEntity;

import java.util.List;

public interface TrafficDirectionService {
    List<TrafficDirectionEntity> findAll();
    TrafficDirectionEntity findById(int id);
    TrafficDirectionEntity findByName(String name);
}

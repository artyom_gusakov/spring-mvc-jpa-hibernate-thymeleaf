package ru.gusakov.services;


import ru.gusakov.domain.TrafficStatisticEntity;
import ru.gusakov.dto.TrafficStatisticRequest;

import java.text.ParseException;
import java.util.List;

public interface TrafficStatisticService {
    List<TrafficStatisticEntity> getTrafficStatistic(TrafficStatisticRequest request) throws ParseException;
}

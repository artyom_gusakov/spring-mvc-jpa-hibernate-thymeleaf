# README #

Проект формирования отчетности по пропускной способности абонента

# Описание #

Имеется БД MySQL, в которой хранятся записи прохождения трафика по абонентам оператора связи в формате:
дата с точностью до секунд; идентификатор абонента; количество трафика, переданного в направлении uplink (байт); количество трафика, переданного в направлении downlink(байт).
Приложение, состоящее из web страницы. На странице выбираются параметры: начало отсчета, окончание отсчета, абонент (предоставить выбор из существующих, пользователь не знает идентификаторов абонентов), направление uplink или downlink.
Программа по заданным параметрам должна отобразить объем переданного трафика и пропускную способность в бит/с для указанного промежутка времени.

# Как установить #

Для развертывания приложения необходимо:

1.Развернуть БД из файла [test.sql](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/test.sql)

2.Скачать Apache [TomCat](https://tomcat.apache.org/download-80.cgi) для своей ОС

3.Установить [Apache TomCat](https://tomcat.apache.org/tomcat-8.0-doc/setup.html)

4.Настроить [менеджер приложений](https://tomcat.apache.org/tomcat-8.0-doc/manager-howto.html#Configuring_Manager_Application_Access) ApacheTomcat

5.Загрузить приложение при помощи менеджера приложений:

  * Перейти по адресу http://localhost:8080/manager - ввести логин и пароль администратора (настройка производилась в п.3.)
  * В секции "WAR file to deploy" выбрать скачаный war файл ([springsite.war](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/springsite.war)) и нажать кнопку Deploy
  
6.Для просмотра приложения пререйти по адресу http://localhost:8080/springsite

# Тестовые данные #

Тестовые данные в БД, доступны в диапазоне 27.11.2017 14:20 - 30.11.2017 14:49:36

# Пример интерфейса #

## Итерфейс системы ##

![Alt-Интерфейс системы](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/inteface.PNG "Итерфейс системы")

------------------------------------------------------------------------------

## Выбор даты ##

![Alt-Выбор даты и времени](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/datetimepiker.png "Выбор даты и времени")

------------------------------------------------------------------------------

## Выбор времени ##

![Alt-Выбор даты и времени](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/datetimepiker1.png "Выбор даты и времени")

------------------------------------------------------------------------------

## Успешный результат поиска ##

![Alt-Успешный результат поиска](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/result.PNG "Успешный результат поиска")

------------------------------------------------------------------------------

## Если нет статистики ##

![Alt-Если нет статистики](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/noresultPNG.PNG "Если нет статистики")

------------------------------------------------------------------------------

# Примеры ошибок #

## Если не введены даты ##

![Alt-Если не введены даты](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/inputerror.PNG "Если не введены даты")

------------------------------------------------------------------------------

## Если начало периода больше окончания ##

![Alt-Если начало периода больше окончания](https://bitbucket.org/artyom_gusakov/spring-mvc-jpa-hibernate-thymeleaf/downloads/error.PNG "Если начало периода больше окончания")